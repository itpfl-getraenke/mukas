import sqlite3
import sys

def parse_args(key):
    if (key == "dbfile"):
        if len(sys.argv) > 1:
            return sys.argv[1]
        else:
            return "../db.sqlite"


def execute(command, conn=None):
    if conn is None:
        conn = sqlite3.connect(parse_args("dbfile"))
    c = conn.cursor()

    # Create table
    x = c.execute(command)

    # Save (commit) the changes
    conn.commit()

    return (x, conn)


def close(conn):
    conn.close()
