#!/usr/bin/env python3

# parameter ist itemid wenn method in (buy, recharge)
# parameter ist userid wenn method in (transferFrom, transferTo)
# parameter ist 0 bei set_balance

# wenn man ein getränk zurück gibt, wird das nicht richtig geloggt. von daher wird es
# hier auch nicht berücksichtigt
# anzahl aller gekauften getränke der letzten zwei wochen:
# select count(l.parameter), i.name from log as l join item as i on i.id like l.parameter where l.method like "buy" and l.time BETWEEN datetime("now", "-14 days") and datetime("now", "localtime") group by l.parameter;
