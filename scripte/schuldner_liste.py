#!/usr/bin/env python3

from helper import parse_args, execute, close

data, conn = execute("select name, balance from user order by balance")

def r(x):
    return str((x[1]*-1)/100)

prefix = "Summe: "

print("Schuldenliste:")
liste = []
length_n = len(prefix)
length_s = 0
for row in data:
    if len(row[0]) > length_n: length_n = len(row[0])
    if len(r(row)) > length_s: length_s = len(r(row))
    liste.append(row)

data, conn = execute("select sum(balance) from user", conn)

for row in liste:
    s = row[0]
    s += " "*(length_n-len(row[0]))
    s2 = " "*(length_s-len(r(row)))
    s2 += str(r(row))
    print(s, s2, "€")

print("-"*20)
print(prefix," "*(length_n-len(prefix)-1), r((None,data.fetchone()[0])), "€")

close(conn)
