#!/usr/bin/env python3.7
from server import *

if __name__ == '__main__':
    # app.run(threaded=True, host="127.0.0.1")
    app.run(threaded=True, host="0.0.0.0")


# vim: et ts=4 sw=4
