# Running
Start `run.py`. Configuration is read from `config.py`.

# Development
Run tests by executing `tests.py`.

For coverage testing, run: `rm .coverage; python -m coverage run tests.py; python -m coverage html` (requires coverage installed)

Vim settings for indentation: `set noet ts=8 sw=8`

# Docker
- `sudo docker build -t mukas .`
    - Build the container
- `sudo docker run --name mukas -p 5000:5000 -v $(pwd)/db:/mukas/db -d mukas`
    - Create and run a container
- `sudo docker stop mukas`
    - Stop mukas
- `sudo docker start mukas`
    - Start mukas
- `sudo docker rm mukas`
    - Remove mukas container

# Docker debugging
- `docker exec -it mukas bash`
    - Start a bash in the container
- `docker logs --follow mukas`
    - Show the logs / output of mukas


This is a fork of:
[gitlab.aachen.ccc.de/mukas/mukas](https://gitlab.aachen.ccc.de/mukas/mukas)
