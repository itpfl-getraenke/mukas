FROM debian:stretch

RUN apt-get update
RUN apt-get install -y sqlite3 locales-all python3-flask python3-ldap3 python3-requests python3-pillow python3-coverage python3-pip
RUN pip3 install Flask-BasicAuth
# RUN python3 -m coverage run tests.py
# RUN python3 -m coverage report --include "./*"
# RUN python3 -m coverage report -m  --include "./*" > report.txt
# RUN python3 -m coverage html --include "./*"

ADD ./ /mukas

WORKDIR /mukas
CMD python3 run.py

EXPOSE 5000
VOLUME /mukas/db
